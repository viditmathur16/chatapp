const mute = require('immutable');
// const async = require('async');
// const moment = require('moment');
const security = require('./../helpers/security');

let logger = require('../helpers/logger');
let genlog = logger.getLogger("server-logs")
let userSchema = require('./../models/mongo/users')
// IPC
const teade = require('teade'); // used to communicate with services

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "users",
    action: null,
    id: null,
    data: null,
    status: null
});

const log = mute.Map({
    location: "controllers",
    file: "users.js"
})

// Login
const login = function (data, cb) {
    if (data) {
        let query = { user_id: data.user_id }
        userSchema.find(query, (err, result) => {
            if (err) {
                return cb(null, responseStruct.merge({
                    success: false,
                    message: "no user found with this Id",
                    status: 404
                }).toJS());
            }
            let password = result.password
            security.comparePassword(data.password, password, (err, result) => {
                if (err) {
                    return cb(err, responseStruct.merge({
                        success: false,
                        message: `error occured while hashing`,
                        status: 500
                    }).toJS())
                }
                else {
                    if (result==true) {
                        return cb(null, responseStruct.merge({
                            success: true,
                            message: `Welcome ${data.user_id}`,
                            status: 302
                        }).toJS());
                    }

                    else {
                        return cb(null, responseStruct.merge({
                            success: false,
                            message: `password incorrect for given user`,
                            status: 401
                        }).toJS());
                    }
                }
            })
        })
    }
    else return cb(null, responseStruct.merge({
        success: false,
        message: "incomplete request ",
        status: 400
    }).toJS());
};


exports.login = login;


// Signup
const signup = function (data, cb) {
    console.log("1");
    
    if (data) {
        let query = { user_id: data.user_id }
        userSchema.find(query, (err, result) => {
            if (err) {
                return cb(responseStruct.merge({
                    success: false,
                    message: "no user found with this Id",
                    status: 404
                }).toJS());
            }
            if (result.length > 0) {
                return cb(responseStruct.merge({
                    success: false,
                    message: "user already exists in db with this user_id",
                    status: 302
                }).toJS())
            }
            security.generatePassword(data.password, (err, result) => {
                if (err) {
                    return cb( responseStruct.merge({
                        success: false,
                        message: `error occured while hashing`,
                        status: 500
                    }).toJS())
                }
                else {
                    console.log("hello");
                    
                    let password = result.hash;
                    let user_id = data.user_id;
                    let is_active = true;
                    let user_data = new userSchema({
                        user_id: user_id,
                        password: password,
                        is_active: is_active
                    })
                    user_data.save().then(data=>{
                        return cb(null,responseStruct.merge({
                            success:true,
                            message:`entry created in user collection`,
                            status:201,
                            data: data
                        }).toJS())
                    }).catch(err=>{
                        return cb(responseStruct.merge({
                            success: false,
                            message: err.message,
                            status: 500 ,
                            data:err           
                        }).toJS())
                    })
                }
            })
        })
    }
    else return cb(null, responseStruct.merge({
        success: false,
        message: "incomplete request ",
        status: 400
    }).toJS());
};

exports.signup = signup