const mute = require('immutable');
// const async = require('async');
// const moment = require('moment');
const security = require('./../helpers/security');


let logger = require('../helpers/logger');
let genlog = logger.getLogger("server-logs")
let conversationSchema = require('./../models/mongo/conversations')
// IPC
const teade = require('teade'); // used to communicate with services

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "users",
    action: null,
    id: null,
    data: null,
    status: null
});

const log = mute.Map({
    location: "controllers",
    file: "conversations.js"
})


// getAll messages
const getAll = function (data, cb) {
    if (data) {
        let query = { conversation_id: data.conversation_id }
        conversationSchema.find(query, (err, result) => {
            if (err) {
                return cb(responseStruct.merge({
                    success: false,
                    message: "error in finding entry",
                    status: 500
                }).toJS());
            }
            if (result.length > 0) {
                return cb(null, responseStruct.merge({
                    success: true,
                    message: `chats found for this conversation`,
                    status: 302,
                    data:result
                }).toJS());
            }
            else {
                return cb(null, responseStruct.merge({
                    success: false,
                    message: `no chat found for this conversation`,
                    status: 404,
                    data:[]
                }).toJS());
            }
        })
    }
    else return cb( responseStruct.merge({
        success: false,
        message: "incomplete request ",
        status: 400
    }).toJS());
};


exports.getAll = getAll;


// get message by message_id
const getById = function (data, cb) {
    console.log("1");

    if (data) {
        let query = { conversation_id: data.conversation_id , message_id: data.message_id }
        conversationSchema.find(query, (err, result) => {
            if (err) {
                return cb(responseStruct.merge({
                    success: false,
                    message: "internal error occured while finiding the message",
                    status: 500
                }).toJS());
            }
            if (result.length > 0) {
                return cb(null, responseStruct.merge({
                    success: true,
                    message: "message found with this Id",
                    status: 302,
                    data:result
                }).toJS())
            }
            else{
                return cb(responseStruct.merge({
                    success: false,
                    message: "message not found with this Id",
                    status: 404,
                    data:[]
                }).toJS())    
            }
        })
    }
    else return cb(null, responseStruct.merge({
        success: false,
        message: "incomplete request ",
        status: 400
    }).toJS());
};

exports.getById = getById

// posting a new message
const sendMessage = function (data, cb) {
    // data.socket
    if (data) {
        let query = { conversation_id: data.conversation_id , message_id: data.message_id }
        conversationSchema.find(query, (err, result) => {
            if (err) {
                return cb(responseStruct.merge({
                    success: false,
                    message: "internal error occured while finiding the message",
                    status: 500
                }).toJS());
            }
            if (result.length > 0) {
                return cb(responseStruct.merge({
                    success: false,
                    message: "message exists with this Id",
                    status: 302,
                    data:result
                }).toJS())
            }
            else{
                let conversation_id = data.conversation_id
                let message_id = data.message_id;
                let sender_id = data.sender_id;
                let reciever_id = data.reciever_id;
                let message_text = data.message_text;

                let conversation_details = new conversationSchema({
                    conversation_id: conversation_id,
                    message_id: message_id,
                    message_text: message_text,
                    reciever_id:reciever_id,
                    sender_id:sender_id
                })
                conversation_details.save().then(data=>{
                    // postSocketMessage(data);
                    //socket.emit("user_id_message")
                    return cb(null, responseStruct.merge({
                        success: true,
                        message: "message saved to db",
                        status: 201,
                        data: data
                    }).toJS())    
                    
                }).catch(err=>{
                    return cb(responseStruct.merge({
                        success: false,
                        message: " error occured  while saving message to the database",
                        status: 500
                    }).toJS())    

                })
            }
        })
    }
    else return cb(null, responseStruct.merge({
        success: false,
        message: "incomplete request ",
        status: 400
    }).toJS());
};
 
exports.sendMessage = sendMessage