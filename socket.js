let app = require('http').createServer(handler)
let io = require('socket.io')(app);
let fs = require('fs');

const conversationController = require('./controllers/conversations');

app.listen(3000);

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }

            res.writeHead(200);
            res.end(data);
        });
}

io.on('connection', function (socket) {
    //socket.emit('news', { hello: 'world' });
    //console.log("Connected",socket)
    socket.on('message', function (data) {
        console.log("socket Id",socket);
        conversationController.sendMessage(data, (err, response) => {
            let status;
            if (err) {
                console.log(err, "err");
                status = err.status;
                throw err;
            }
            console.log(response, "resp");
            status = response.status;
            console.log("in socket",status)
            io.emit(`${response.data.reciever_id}_message`, data);
            io.emit(`${response.data.sender_id}_message`, data);
        })
    });
});