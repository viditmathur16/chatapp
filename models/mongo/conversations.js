let mongoose = require('./db');

// grab the things we need
let Schema = mongoose.Schema;

// create a schema
let conversationSchema = new Schema({
    conversation_id: String,
    message_id: String,
    message_text: String,
    sender_id: String,
    reciever_id: String,
    created_at: Date
});

conversationSchema.pre('save', function(next) {
    // get the current date
    let current_date = new Date();

    // change the updated_at field to current date
    // this.updated_at = current_date;

    // if created_at doesn't exist, add to that field
    if (!this.created_at){
        this.created_at = current_date;
    }
    next();
});
// the schema is useless so far
// we need to create a model using it
let env = process.env.DB_ENV || "development";
let conversation = mongoose.model(`conversation_${env}`, conversationSchema);

module.exports = conversation;
