/**
 * Database config and initialization
 */

const mongoose = require('mongoose');
const host = process.env.ODB_HOST;
const port = process.env.ODB_PORT;
const user = process.env.ODB_USER;
const pass = process.env.ODB_PASS;
const db = process.env.ODB_DB;

const logger = require('../../helpers/logger');
let genlog = logger.getLogger("server-logs")

const opt = {
    // user: user,
    // pass: pass,
    // authSource: "admin",
    useNewUrlParser: true,
    keepAlive: 1,
    reconnectTries: 30,
    connectTimeoutMS: 10000,
    socketTimeoutMS: (process.env.NODE_ENV === "production" ? 3000000 : 30000),
};

const connstring = `mongodb://${host}:${port}/${db}`;

mongoose.connect(connstring, opt, function(err) {
    if (err) {
        genlog.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
        setTimeout(connectWithRetry, 5000);
    }else{
        genlog.info(`Connected to ${db} MongoDB`);
    }
});

module.exports = mongoose;
