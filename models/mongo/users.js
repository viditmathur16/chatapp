let mongoose = require('./db');

// grab the things we need
let Schema = mongoose.Schema;

// create a schema
let userSchema = new Schema({
    userId: String,
    password: String,
    isActive: Boolean,
    created_at: Date,
    updated_at: Date
});

userSchema.pre('save', function(next) {
    // get the current date
    let currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at){
        this.created_at = currentDate;
    }

    // if last_accessed doesn't exist, add to that field
    if (!this.last_accessed){
        this.last_accessed = currentDate;
    }
    next();
});
// the schema is useless so far
// we need to create a model using it
let env = process.env.DB_ENV || "development";
let user = mongoose.model(`user_${env}`, userSchema);

module.exports = user;
