const teade = require('teade');

let logger = require('./helpers/logger');
let genlog = logger.getLogger("server-logs");

let server = new teade.Server();
server.addService({
    'ping': ping
});

server.bind(process.env.SERVICE_RPC_PORT);
genlog.info("chat RPC Server started at port: " + process.env.SERVICE_RPC_PORT);
console.log("chat RPC Server started at port: " + process.env.SERVICE_RPC_PORT);
server.start();

function ping(call, callback) {
    callback(null, "pong")
};

module.exports = server;