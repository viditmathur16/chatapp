if (process.env.ENV != "production") {
  require('dotenv').config();
}

const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');

// Routes imports
const index = require('./routes/index');
const users = require('./routes/users');
const conversations = require('./routes/conversations');

let app = express();

app.set('trust proxy', true);
app.use(helmet());


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Healthcheck
app.use('/', index);
// Routes
app.use('/users', users);
app.use('/conversations',conversations);

//serving static page
app.use('/views',express.static(__dirname +'public'))
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({
    success: false,
    message: res.locals.message,
    error: res.locals.error
  });
});

require('./server');
require('./socket');


module.exports = app;
