const winston = require('winston');

// server start time
const startTime = new Date().toISOString();

let loggers = {}
loggers['console-logs'] = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console({
            json: true,
            colorize: true,
            level: 'info'
        })
    ]
});
loggers['server-logs'] = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console({
            json: true,
            colorize: true,
            level: 'info'
        })
    ]
});
// loggers['server-logs'] = winston.createLogger({
//     level: 'info',
//     format: winston.format.json(),
//     transports: [
//         new winston.transports.Console({
//             json: true,
//             colorize: true,
//             level: 'info',
//             silent: true
//         }),
//         new WinstonCloudWatch({
//             logGroupName: 'pleb-users-server-logs',
//             logStreamName: function() {
//                 // Spread log streams across dates as the server stays up
//                 let date = new Date().toISOString().split('T')[0];
//                 return 'es-' + date + '-' +
//                 crypto.createHash('md5')
//                 .update(startTime)
//                 .digest('hex');
//             },
//             awsRegion: 'us-east-1',
//             jsonMessage: true
//         })
//     ]
// });
if(process.env.NODE_ENV === "production"){
    console.log("Logging directly to CloudWatch.");
}else{
    console.log("Logging to Console.");
}
exports.getLogger = function(type){
    if(process.env.NODE_ENV === "production"){
        if(!loggers[type]){
            return loggers['console-logs'];
        }
        return loggers[type];
    }else{
        return loggers['console-logs'];
    }
}