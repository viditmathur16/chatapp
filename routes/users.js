const express = require('express');
const router = express.Router();

const logger = require('../helpers/logger');
let genlog = logger.getLogger("server-logs")

const users= require('./../controllers/users')
/* Middlewares */

const clients = {
  users: {
      host: process.env.SERVICE_RPC_HOST,
      port: process.env.PB_USER_PORT
  }
}
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

// /* POST user logins. */
router.post('/v1/users/login', function(req, res, next) {
    // console.log(next);
    
    let data = req.body;
    data.req = req.data;

    users.login(data, function(err, response) {
      var status = 0;
      if (err) { 
        status = err.status;
        return res.status(status).send(err);
      }
      
      console.log(response,"resp");
      status = response.status;
      return res.status(status).send(response);
    });
});

// /* POST user signup. */
router.post('/v1/users/signup', function(req, res, next) {
  // console.log(next);
  
  let data = req.body;
  data.req = req.data;

  users.signup(data, function(err, response) {
    // var status = response.status||err.status||500;
    if (err) { 
      console.log(err,"err");
      status = err.status;
      return res.status(status).send(err);
    }
    console.log(response,"resp");
    status = response.status;
    return res.status(status).send(response);
  });
});

module.exports = router;