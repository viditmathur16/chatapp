const express = require('express');
const router = express.Router();

const logger = require('../helpers/logger');
let genlog = logger.getLogger("server-logs")

const conversations= require('./../controllers/conversations')
/* Middlewares */

const clients = {
  users: {
      host: process.env.SERVICE_RPC_HOST,
      port: process.env.PB_USER_PORT
  }
}
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

// /* get all conversations. */
router.get('/v1/conversations/', function(req, res, next) {
    // console.log(next);
    
    let data = req.body;
    data.req = req.data;

    conversations.getAll(data, function(err, response) {
      var status = 0;
      if (err) { 
        status = err.status;
        return res.status(status).send(err);
      }
      console.log(response,"resp");
      status = response.status;
      return res.status(status).send(response);
    });
});

// /* get conversation by messageId. */
router.get('/v1/conversations/:id', function(req, res, next) {
  // console.log(next);
  
  let data = req.body;
  data.message_id= req.params.id
  data.req = req.data;

  conversations.getById(data, function(err, response) {
    // var status = response.status||err.status||500;
    if (err) { 
      console.log(err,"err");
      status = err.status;
      return res.status(status).send(err);
    }
    console.log(response,"resp");
    status = response.status;
    return res.status(status).send(response);
  });
});

// /* posting new message to conversation. */
router.post('/v1/conversations/', function(req, res, next) {
    // console.log(next);
    
    let data = req.body;
    data.req = req.data;
  
    conversations.sendMessage(data, function(err, response) {
      // var status = response.status||err.status||500;
      if (err) { 
        console.log(err,"err");
        status = err.status;
        return res.status(status).send(err);
      }
      console.log(response,"resp");
      status = response.status;
      return res.status(status).send(response);
    });
  });
  
module.exports = router;