var express = require('express');
var router = express.Router();

var health = require('../middlewares/health');
router.use(health);

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);


let healthResponse = {
    title: "Test Chat App",
    service: "chat",
    health: "ok",
    env: process.env.DB_ENV
};

/* GET home page. */
router.get('/', function(req, res, next) {
    res.status(200).send(healthResponse);
});
router.get('/chat/v1/health', function(req, res, next) {
    res.status(200).send(healthResponse);
});

module.exports = router;